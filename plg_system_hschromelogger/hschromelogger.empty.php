<?php
/**
 * @package     HSChromeLogger!
 * @author      Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CL {
    /**
     * Enable and disable logging
     *
     * @param   boolean  $enabled  TRUE to enable, FALSE to disable
     *
     * @return  void
     */
    public static function setEnabled($enabled) {
    }

    /**
     * Check if logging is enabled
     *
     * @return  boolean  TRUE if enabled
     */
    public static function getEnabled() {
        return false;
    }

    /**
     * logs a variable to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function log() {
    }

    /**
     * logs a warning to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function warn() {
    }

    /**
     * logs an error to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function error() {
    }

    /**
     * sends an info log
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function info() {
    }

    /**
     * sends a group log
     *
     * @param  string  value
     *
     * @return  void
     */
    public static function group() {
    }

    /**
     * sends a collapsed group log
     *
     * @param  string  value
     *
     * @return  void
     */
    public static function groupCollapsed() {
    }

    /**
     * ends a group log
     *
     * @param  string  value
     *
     * @return  void
     */
    public static function groupEnd() {
    }

    /**
     * sends a table log
     *
     * @param  string  value
     *
     * @return  void
     */
    public static function table() {
    }

    /**
     * HSChromeLogger's error handler
     *
     * Throws exception for each php error that will occur.
     *
     * @param  int     $errno
     * @param  string  $errstr
     * @param  string  $errfile
     * @param  int     $errline
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline) {
    }

    /**
     * HSChromeLogger's exception handler
     *
     * Logs all exceptions to your web console and then stops the script.
     *
     * @param   Exception  $Exception
     *
     * @throws  Exception
     */
    public static function exceptionHandler($Exception) {
    }

    /**
     * HSChromeLogger's assertion handler
     *
     * Logs all assertions to your web console.
     *
     * @param  string  $file  File source of assertion
     * @param  int     $line  Line source of assertion
     * @param  string  $code  Assertion code
     * @param  string  $desc  Assertion description
     */
    public static function assertionHandler($file, $line, $code, $desc = null) {
    }
}