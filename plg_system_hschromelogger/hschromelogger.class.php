<?php
/**
 * @package     HSChromeLogger!
 * @author      Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 *
 * Starting line 281 is the class ChromePhp
 *      v4.1.0 Copyright 2010-2013 Craig Campbell (https://github.com/ccampbell/chromephp)
 *      v4.3.0 Copyright 2016      Erik Krause (https://github.com/ErikKrause/chromephp)
 *             Copyright 2014      Elijah Madden (https://github.com/okonomiyaki3000/chromephp/commit/3d1bd4d7bd5244a6ab1bedeb2057feeb42574ae5)
 *             Copyright 2017      Fabrício Seger Kolling (https://github.com/dulldusk/chromephp/commit/171e1527df2a11bf1ba457780e65c9c32ed4bc02)
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CL {
    /**
     * Enable and disable logging
     *
     * @param   boolean  $enabled  TRUE to enable, FALSE to disable
     *
     * @return  void
     */
    public static function setEnabled($enabled) {
        ChromePhp::getInstance()->setEnabled($enabled);
        call_user_func('self::queue', 'setEnabled', $enabled);
    }

    /**
     * Check if logging is enabled
     *
     * @return  boolean  TRUE if enabled
     */
    public static function getEnabled() {
        return ChromePhp::getInstance()->getEnabled();
    }

    /**
     * logs a variable to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function log() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'log'), $args);
        array_unshift($args, 'log');
        call_user_func_array('self::queue', $args);
    }

    /**
     * logs a warning to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function warn() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'warn'), $args);
        array_unshift($args, 'warn');
        call_user_func_array('self::queue', $args);
    }

    /**
     * logs an error to the console
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function error() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'error'), $args);
        array_unshift($args, 'error');
        call_user_func_array('self::queue', $args);
    }

    /**
     * sends an info log
     *
     * @param   mixed  $data,... unlimited OPTIONAL number of additional logs [...]
     *
     * @return  void
     */
    public static function info() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'info'), $args);
        array_unshift($args, 'info');
        call_user_func_array('self::queue', $args);
    }

    /**
     * sends a group log
     *
     * @param  string  value
     */
    public static function group() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'group'), $args);
        array_unshift($args, 'group');
        call_user_func_array('self::queue', $args);
    }

    /**
     * sends a collapsed group log
     *
     * @param  string  value
     */
    public static function groupCollapsed() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'groupCollapsed'), $args);
        array_unshift($args, 'groupCollapsed');
        call_user_func_array('self::queue', $args);
    }

    /**
     * ends a group log
     *
     * @param  string  value
     */
    public static function groupEnd() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'groupEnd'), $args);
        array_unshift($args, 'groupEnd');
        call_user_func_array('self::queue', $args);
    }

    /**
     * sends a table log
     *
     * @param  string  value
     */
    public static function table() {
        $instance = ChromePhp::getInstance();
        $args = func_get_args();
        call_user_func_array(array($instance, 'table'), $args);
        array_unshift($args, 'table');
        call_user_func_array('self::queue', $args);
    }

    /**
     * Queue using JSession
     *
     * @param   mixed  $object
     *
     * @return  void
     */
    private static function queue() {
        $session = JFactory::getApplication()->getSession();
        $queue   = $session->get('hschromelogger.queue', array());
        $queue[] = func_get_args();
        $session->set('hschromelogger.queue', $queue);
    }

    /**
     * HSChromeLogger's error handler
     *
     * Throws exception for each php error that will occur.
     *
     * @param  int     $errno
     * @param  string  $errstr
     * @param  string  $errfile
     * @param  int     $errline
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline) {
        // Don't throw exception if error reporting is switched off
        if (error_reporting() == 0) {
            return;
        }
        // Only throw exceptions for errors we are asking for
        if (error_reporting() & $errno) {
            $errcon = '';
            switch($errno) {
                case E_WARNING:
                    $errcon = 'E_WARNING:';
                    break;
                case E_NOTICE:
                    $errcon = 'E_NOTICE:';
                    break;
                case E_USER_ERROR:
                    $errcon = 'E_USER_ERROR:';
                    break;
                case E_USER_WARNING:
                    $errcon = 'E_USER_WARNING:';
                    break;
                case E_USER_NOTICE:
                    $errcon = 'E_USER_NOTICE:';
                    break;
                case E_STRICT:
                    $errcon = 'E_STRICT:';
                    break;
                case E_RECOVERABLE_ERROR:
                    $errcon = 'E_RECOVERABLE_ERROR:';
                    break;
                case E_DEPRECATED:
                    $errcon = 'E_DEPRECATED:';
                    break;
                case E_USER_DEPRECATED:
                    $errcon = 'E_USER_DEPRECATED:';
                    break;
            }
            $instance = ChromePhp::getInstance();
            call_user_func(array($instance, 'error'), $errcon, $errstr, new CLbacktrace($errfile, $errline));
            call_user_func('self::queue', $errcon, $errstr, new CLbacktrace($errfile, $errline));
        }
    }

    /**
     * HSChromeLogger's exception handler
     *
     * Logs all exceptions to your web console and then stops the script.
     *
     * @param   Exception  $Exception
     *
     * @throws  Exception
     */
    public static function exceptionHandler($Exception) {
        $instance = ChromePhp::getInstance();
        call_user_func(array($instance, 'error'), '', $Exception->getMessage(), new CLbacktrace($Exception->getFile(), $Exception->getLine()));
    }

    /**
     * HSChromeLogger's assertion handler
     *
     * Logs all assertions to your web console.
     *
     * @param  string  $file  File source of assertion
     * @param  int     $line  Line source of assertion
     * @param  string  $code  Assertion code
     * @param  string  $desc  Assertion description
     */
    public static function assertionHandler($file, $line, $code, $desc = null) {
        $instance = ChromePhp::getInstance();
        call_user_func(array($instance, 'error'), 'ASSERTION FAILED:', $code.$desc, new CLbacktrace($file, $line));
        call_user_func('self::queue', 'ASSERTION FAILED:', $code.$desc, new CLbacktrace($file, $line));
    }
}

class CLbacktrace {
    /**
     * @var  array  The backtrace.
     */
    private $backtrace;

    /**
     * Constructor
     *
     * @param  string  $file  The file name.
     * @param  int     $line  The line number.
     */
    public function __construct($file, $line) {
        $this->backtrace = array('file' => $file, 'line' => $line);
    }

    /**
     * Get the backtrace.
     *
     * @return  array  The backtrace.
     */
    public function getBacktrace() {
        return $this->backtrace;
    }
}

/**
 * Copyright 2010-2013 Craig Campbell, 2016 Erik Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Server Side Chrome PHP debugger class
 *
 * @package ChromePhp
 * @author Craig Campbell <iamcraigcampbell@gmail.com>
 * @author Erik Krause <erik.krause@gmx.de>
 * @author Elijah Madden
 * @author Fabrício Seger Kolling <fabricio@dulldusk.com>
 * @author Dr. Yves Kreis <yves@hosting-skills.lu>
 */
class ChromePhp
{
    /**
     * @var string (ek)
     */
    const VERSION = '4.3.0';

    /**
     * @var string
     */
    const HEADER_NAME = 'X-ChromeLogger-Data';

    /**
     * @var string
     */
    const BACKTRACE_LEVEL = 'backtrace_level';

    /**
     * @var string (ek)
     */
    const LOG_STYLE = 'log_style';

    /**
     * @var string (yk:em)
     */
    const BASE_PATH = 'base_path';

    /**
     * @var string
     */
    const LOG = 'log';

    /**
     * @var string
     */
    const WARN = 'warn';

    /**
     * @var string
     */
    const ERROR = 'error';

    /**
     * @var string
     */
    const GROUP = 'group';

    /**
     * @var string
     */
    const INFO = 'info';

    /**
     * @var string
     */
    const GROUP_END = 'groupEnd';

    /**
     * @var string
     */
    const GROUP_COLLAPSED = 'groupCollapsed';

    /**
     * @var string
     */
    const TABLE = 'table';

    /**
     * @var int (yk:fsk)
     *
     * https://httpd.apache.org/docs/2.4/mod/core.html#limitrequestfieldsize
     */
    const HTTPD_HEADER_LIMIT = 8190;

    /**
     * @var string
     */
    protected $_php_version;

    /**
     * @var int
     */
    protected $_timestamp;

    /**
     * @var array
     */
    protected $_json = array(
        'version' => self::VERSION,
        'columns' => array('log', 'backtrace', 'type'),
        'rows' => array()
    );

    /**
     * @var array
     */
    protected $_backtraces = array();

    /**
     * @var bool
     */
    protected $_error_triggered = false;

    /**
     * @var array
     */
    protected $_settings = array(
        self::BACKTRACE_LEVEL => 1
    );

    /**
     * @var ChromePhp
     */
    protected static $_instance;

    /**
     * Prevent recursion when working with objects referring to each other
     *
     * @var array
     */
    protected $_processed = array();

    /**
     * provide enabled / disabled state
     *
     * @var bool
     */
    protected $_enabled = true;

    /**
     * constructor
     */
    private function __construct()
    {
        $this->_php_version = phpversion();
        $this->_timestamp = $this->_php_version >= 5.1 ? $_SERVER['REQUEST_TIME'] : time();
        $this->_json['request_uri'] = $_SERVER['REQUEST_URI'];
    }

    /**
     * Enable and disable logging (ek)
     *
     * @param boolean $Enabled TRUE to enable, FALSE to disable
     * @param string  $style 'FirePHP' to switch to FirePHP behaviour
     * @return void
     */
    public function setEnabled($Enabled, $style = '')
    {
       $this->_enabled = $Enabled;
       if ($style)
         $this->addSetting(self::LOG_STYLE, $style);
    }

    /**
     * Check if logging is enabled
     *
     * @return boolean TRUE if enabled
     */
    public function getEnabled()
    {
        return $this->_enabled;
    }


    /**
     * gets instance of this class
     *
     * @return ChromePhp
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * logs a variable to the console
     *
     * @param mixed $data,... unlimited OPTIONAL number of additional logs [...]
     * @return void
     */
    public static function log()
    {
        $args = func_get_args();
        return self::_log('', $args);
    }

    /**
     * logs a warning to the console
     *
     * @param mixed $data,... unlimited OPTIONAL number of additional logs [...]
     * @return void
     */
    public static function warn()
    {
        $args = func_get_args();
        return self::_log(self::WARN, $args);
    }

    /**
     * logs an error to the console
     *
     * @param mixed $data,... unlimited OPTIONAL number of additional logs [...]
     * @return void
     */
    public static function error()
    {
        $args = func_get_args();
        return self::_log(self::ERROR, $args);
    }

    /**
     * sends a group log
     *
     * @param string value
     */
    public static function group()
    {
        $args = func_get_args();
        return self::_log(self::GROUP, $args);
    }

    /**
     * sends an info log
     *
     * @param mixed $data,... unlimited OPTIONAL number of additional logs [...]
     * @return void
     */
    public static function info()
    {
        $args = func_get_args();
        return self::_log(self::INFO, $args);
    }

    /**
     * sends a collapsed group log
     *
     * @param string value
     */
    public static function groupCollapsed()
    {
        $args = func_get_args();
        return self::_log(self::GROUP_COLLAPSED, $args);
    }

    /**
     * ends a group log
     *
     * @param string value
     */
    public static function groupEnd()
    {
        $args = func_get_args();
        return self::_log(self::GROUP_END, $args);
    }

    /**
     * sends a table log
     *
     * @param string value
     */
    public static function table()
    {
        $args = func_get_args();
        return self::_log(self::TABLE, $args);
    }

    /**
     * internal logging call
     *
     * @param string $type
     * @return void
     */
    protected static function _log($type, array $args)
    {
        // nothing passed in, don't do anything
        if (count($args) == 0 && $type != self::GROUP_END) {
            return;
        }


        $logger = self::getInstance();

        // not enabled, don't do anything (ek)
        if (!($logger->_enabled))
            return;

        $logger->_processed = array();

        // FirePHP passes the object name second but displays it first (ek)
        if (($logger->getSetting(self::LOG_STYLE) == 'FirePHP') && (count($args) == 2)) {
            $args = array_reverse($args);
            $args[0] .= ":";
        }

        // Move debug_backtrace before loop over args (yk)
        $backtrace = debug_backtrace(false);
        $level = $logger->getSetting(self::BACKTRACE_LEVEL);
        // BASE_PATH (yk:em)
        $basepath = $logger->getSetting(self::BASE_PATH);

        $logs = array();
        foreach ($args as $arg) {
            // If arg is an instance of CLbacktrace, store into backtrace (yk)
            if ($arg instanceof CLbacktrace) {
                $backtrace[$level] = $arg->getBacktrace();
            } else {
                $logs[] = $logger->_convert($arg);
            }
        }

        $backtrace_message = 'unknown';
        if (isset($backtrace[$level]['file']) && isset($backtrace[$level]['line'])) {
            // BASE_PATH (yk:em)
            $file = $backtrace[$level]['file'];

            if ($basepath && strpos($file, $basepath) === 0) {
                $file = '.' . substr($file, strlen($basepath));
            }

            $backtrace_message = $file . ' : ' . $backtrace[$level]['line'];
        }

        $logger->_addRow($logs, $backtrace_message, $type);
    }

    /**
     * converts an object to a better format for logging
     *
     * @param Object
     * @return array
     */
    protected function _convert($object)
    {
        // if this isn't an object then just return it
        if (!is_object($object)) {
            return $object;
        }

        //Mark this object as processed so we don't convert it twice and it
        //Also avoid recursion when objects refer to each other
        $this->_processed[] = $object;

        $object_as_array = array();

        // first add the class name
        $object_as_array['___class_name'] = get_class($object);

        // loop through object vars
        $object_vars = get_object_vars($object);
        foreach ($object_vars as $key => $value) {

            // same instance as parent object
            if ($value === $object || in_array($value, $this->_processed, true)) {
                $value = 'recursion - parent object [' . get_class($value) . ']';
            }
            $object_as_array[$key] = $this->_convert($value);
        }

        $reflection = new ReflectionClass($object);

        // loop through the properties and add those
        foreach ($reflection->getProperties() as $property) {

            // if one of these properties was already added above then ignore it
            if (array_key_exists($property->getName(), $object_vars)) {
                continue;
            }
            $type = $this->_getPropertyKey($property);

            if ($this->_php_version >= 5.3) {
                $property->setAccessible(true);
            }

            try {
                $value = $property->getValue($object);
            } catch (ReflectionException $e) {
                $value = 'only PHP 5.3 can access private/protected properties';
            }

            // same instance as parent object
            if ($value === $object || in_array($value, $this->_processed, true)) {
                $value = 'recursion - parent object [' . get_class($value) . ']';
            }

            $object_as_array[$type] = $this->_convert($value);
        }
        return $object_as_array;
    }

    /**
     * takes a reflection property and returns a nicely formatted key of the property name
     *
     * @param ReflectionProperty
     * @return string
     */
    protected function _getPropertyKey(ReflectionProperty $property)
    {
        $static = $property->isStatic() ? ' static' : '';
        if ($property->isPublic()) {
            return 'public' . $static . ' ' . $property->getName();
        }

        if ($property->isProtected()) {
            return 'protected' . $static . ' ' . $property->getName();
        }

        if ($property->isPrivate()) {
            return 'private' . $static . ' ' . $property->getName();
        }
    }

    /**
     * adds a value to the data array
     *
     * @var mixed
     * @return void
     */
    protected function _addRow(array $logs, $backtrace, $type)
    {
        // if this is logged on the same line for example in a loop, set it to null to save space
        if (in_array($backtrace, $this->_backtraces)) {
            $backtrace = null;
        }

        // for group, groupEnd, and groupCollapsed
        // take out the backtrace since it is not useful
        if ($type == self::GROUP || $type == self::GROUP_END || $type == self::GROUP_COLLAPSED) {
            $backtrace = null;
        }

        if ($backtrace !== null) {
            $this->_backtraces[] = $backtrace;
        }

        $row = array($logs, $backtrace, $type);

        $this->_json['rows'][] = $row;
        $this->_writeHeader($this->_json);
    }

    protected function _writeHeader($data)
    {
        $encodedData = $this->_encode($data);
        if ($encodedData) {
            // HTTPD_HEADER_LIMIT (yk:fsk)
            $header = self::HEADER_NAME . ': ' . $encodedData;
            // Most HTTPD servers have a default header line length limit of 8kb, must test to avoid 500 Internal Server Error.
            if (strlen($header) > self::HTTPD_HEADER_LIMIT) {
                $data['rows'] = array();
                $data['rows'][] = array(array('HSChromeLogger! Error: The HTML header will surpass the limit of '.$this->_formatSize(self::HTTPD_HEADER_LIMIT).' ('.$this->_formatSize(strlen($header)).') - You can increase the HTTPD_HEADER_LIMIT on HSChromeLogger! class, according to your Apache LimitRequestFieldsize directive'), '', self::ERROR);
                $header = self::HEADER_NAME . ': ' . $this->_encode($data);
            }
            header($header);
        }
    }

    // HTTPD_HEADER_LIMIT (yk:fsk)
    protected function _formatSize($arg) {
        if ($arg>0) {
            $j = 0;
            $ext = array("bytes","Kb","Mb","Gb","Tb");
            while ($arg >= pow(1024,$j)) ++$j; {
                $arg = (round($arg/pow(1024,$j-1)*100)/100).($ext[$j-1]);
            }
            return $arg;
        } else return "0Kb";
    }

    /**
     * recursively converts data for json_encode (ek)
     *
     * @param mixed ref $dat
     */

    protected static function _filterArray(&$dat)
    {
        if (is_resource($dat))
            $dat = print_r($dat, true).", ".get_resource_type($dat);
        elseif (is_numeric($dat) && !is_finite($dat))
            $dat = print_r($dat, true) . ", numeric"; // fixes issue #35
        elseif (!is_object($dat) && !is_null($dat) && !is_scalar($dat))
            $dat = print_r($dat, true);
    }

    /**
     * encodes the data to be sent along with the request
     *
     * @param array $data
     * @return string
     */

    protected function _encode($data)
    {

        array_walk_recursive($data, 'self::_filterArray'); //(ek)

        return base64_encode(utf8_encode(json_encode($data)));
    }

    /**
     * adds a setting
     *
     * @param string key
     * @param mixed value
     * @return void
     */
    public function addSetting($key, $value)
    {
        $this->_settings[$key] = $value;
    }

    /**
     * add ability to set multiple settings in one call
     *
     * @param array $settings
     * @return void
     */
    public function addSettings(array $settings)
    {
        foreach ($settings as $key => $value) {
            $this->addSetting($key, $value);
        }
    }

    /**
     * gets a setting
     *
     * @param string key
     * @return mixed
     */
    public function getSetting($key)
    {
        if (!isset($this->_settings[$key])) {
            return null;
        }
        return $this->_settings[$key];
    }
}