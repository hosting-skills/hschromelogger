<?php
/**
 * @package     HSChromeLogger!
 * @author      Hosting Skills a.s.b.l. - http://www.hosting-skills.lu
 * @copyright   Copyright (C) 2014-2021 Hosting Skills a.s.b.l.. All rights reserved.
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * HSChromeLogger! System Plugin Installer Script
 */
class plgSystemHSChromeLoggerInstallerScript
{
    /**
     * Version of the Plugin
     *
     * @var  string
     */
    protected $version;

    /**
     * Runs just before any installation action is preformed on the component.
     * Verifications and pre-requisites should run in this function.
     *
     * @param  string     $type    Type of PreFlight action. Possible values are:
     *                             * install
     *                             * update
     *                             * discover_install
     * @param  \stdClass  $parent  Parent object calling object.
     *
     * @return void
     */
    public function preflight($route, $parent) {
        $jversion = new JVersion();
        $this->version = $parent->getManifest()->version;
        $joomla_version = $parent->getManifest()->attributes()->version;

        if (version_compare($jversion->getShortVersion(), $joomla_version, 'lt')) {
            $parent->getParent()->message = '';
            JFactory::getApplication()->enqueueMessage(sprintf(JText::_('PLG_SYSTEM_HSCHROMELOGGER_JOOMLA_VERSION_ERROR'), $joomla_version), 'error');
            return false;
        }

        if ('update' == $route) {
            $db = $parent->getParent()->getDbo();
            $query = $db->getQuery(true)
                ->select($db->quoteName('manifest_cache'))
                ->from($db->quoteName('#__extensions'))
                ->where($db->quoteName('name') . ' = ' . $db->quote($parent->getManifest()->name));
            $db->setQuery($query);

            try {
                $db->execute();
                $manifest = json_decode($db->loadResult(), true);
                $update_version = $manifest['version'];
            } catch (RuntimeException $e) {
                $update_version = $this->version;
            }

            if (version_compare($this->version, $update_version, 'le')) {
                $parent->getParent()->message = '';
                JFactory::getApplication()->enqueueMessage(sprintf(JText::_('PLG_SYSTEM_HSCHROMELOGGER_UPDATE_VERSION_ERROR'), $update_version, $this->version), 'error');
                return false;
            }
        }
    }

    /**
     * This method is called after a component is installed.
     *
     * @param  \stdClass  $parent  Parent object calling this method.
     *
     * @return  void
     */
    public function install($parent) {
        $parent->getParent()->message = sprintf(JText::_('PLG_SYSTEM_HSCHROMELOGGER_INSTALL'), $this->version).'<br /><br /><span style="color: red;">'.JText::_('PLG_SYSTEM_HSCHROMELOGGER_CONFIGURE').'</span><br /><br /><img src="../plugins/system/hschromelogger/hschromelogger.png" alt="HSChromeLogger!" /><br /><br />';
    }

    /**
     * This method is called after a component is updated.
     *
     * @param  \stdClass  $parent  Parent object calling object.
     *
     * @return  void
     */
    public function update($parent) {
        $db = $parent->getParent()->getDbo();
        $query = $db->getQuery(true)
            ->select($db->quoteName('enabled'))
            ->from($db->quoteName('#__extensions'))
            ->where($db->quoteName('name') . ' = ' . $db->quote($parent->getManifest()->name));
        $db->setQuery($query);

        try {
            $db->execute();
            $enabled = $db->loadResult();
        } catch (RuntimeException $e) {
            $enabled = false;
        }

	    // Fix update servers.
	    if (version_compare($this->version, '1.3.1', 'lt')) {
		    $db = JFactory::getDbo();
		    $query = $db->getQuery(true);
		    $query->delete($db->quoteName('#__update_sites'))
			    ->where($db->quoteName('location') . ' = ' . $db->quote('http://www.hosting-skills.lu/updates/hschromelogger.xml'));
		    $db->setQuery($query);
		    $db->execute();
	    }

	    $parent->getParent()->message = sprintf(JText::_('PLG_SYSTEM_HSCHROMELOGGER_UPDATE'), $this->version).($enabled?'':'<br /><br /><span style="color: red;">'.JText::_('PLG_SYSTEM_HSCHROMELOGGER_CONFIGURE')).'</span><br /><br /><img src="../plugins/system/hschromelogger/hschromelogger.png" alt="HSChromeLogger!" /><br /><br />';
    }

	/**
	 * Runs right after any installation action is preformed on the component.
	 *
	 * @param  string     $type    Type of PostFlight action. Possible values are:
	 *                             * install
	 *                             * update
	 *                             * discover_install
	 * @param  \stdClass  $parent  Parent object calling object.
	 *
	 * @return void
	 */
	function postflight($type, $parent) {
	}

	/**
	 * This method is called after a component is uninstalled.
	 *
	 * @param  \stdClass  $parent  Parent object calling this method.
	 *
	 * @return void
	 */
	public function uninstall($parent) {
	}
}